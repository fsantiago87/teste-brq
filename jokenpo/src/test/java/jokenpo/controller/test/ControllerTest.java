package jokenpo.controller.test;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.boot.SpringBootConfiguration;
import org.springframework.http.MediaType;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.teste.jokenpo.controller.Controller;
import com.teste.jokenpo.model.JogadaRequest;
import com.teste.jokenpo.model.JogadorRequest;
import com.teste.jokenpo.service.JogoService;


@RunWith(SpringRunner.class)
@SpringBootConfiguration
public class ControllerTest {

	private MockMvc mockMvc;
	 
    @InjectMocks
    private Controller controller;
 
    @Mock
    private JogoService jogoService;
    
    @Before
    public void setUp() throws Exception {
        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller).build();
    }
	
	@Test
    public void statusTest() throws Exception {
  
        mockMvc.perform(get("/status")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("OK"));     
        
    }
	
	@Test
    public void consultarJogadoresTest() throws Exception {
  
        mockMvc.perform(get("/consultarJogadores")
                .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(content().string("OK"));     
        
    }
	
	@Test
	public void cadastrarJogadorTest() throws Exception {
		
		JogadorRequest jogadorRequest = new JogadorRequest();
		jogadorRequest.setNumero("7890");
		jogadorRequest.setNome("Josefa");

		mockMvc.perform( MockMvcRequestBuilders
			      .post("/cadastrarJogador")
			      .content(new ObjectMapper().writeValueAsString(jogadorRequest))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(status().isCreated());
		
	}
	
	@Test
	public void alterarJogadorTest() throws Exception {
		
		JogadorRequest jogadorRequest = new JogadorRequest();
		jogadorRequest.setNumero("7890");
		jogadorRequest.setNome("Mara");

		mockMvc.perform( MockMvcRequestBuilders
				.put("/alterarJogador", jogadorRequest.getNumero())
				.content(new ObjectMapper().writeValueAsString(jogadorRequest))
			    .contentType(MediaType.APPLICATION_JSON)
			    .accept(MediaType.APPLICATION_JSON))
			    .andExpect(status().isOk());
			     
		
	}
	
	@Test
	public void jogadaTest() throws Exception {
		
		JogadaRequest jogadaRequest = new JogadaRequest();
		jogadaRequest.setCodigoJogador("7890");
		jogadaRequest.setJogada("pedra");

		mockMvc.perform( MockMvcRequestBuilders
			      .post("/jogada")
			      .content(new ObjectMapper().writeValueAsString(jogadaRequest))
			      .contentType(MediaType.APPLICATION_JSON)
			      .accept(MediaType.APPLICATION_JSON))
			      .andExpect(status().isCreated());		
	}

	@Test
	public void excluirJogadorTest() throws Exception {
		
		String jogadorExcluir = "7890";

		mockMvc.perform( MockMvcRequestBuilders.delete("/excluirJogador/{codigo}", jogadorExcluir) )
        .andExpect(status().isAccepted());
		
		
	}
	
}
