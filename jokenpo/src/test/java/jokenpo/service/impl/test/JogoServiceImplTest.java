package jokenpo.service.impl.test;
  
import org.junit.Test;

import com.teste.jokenpo.model.JogadaRequest;
import com.teste.jokenpo.model.Jogador;
import com.teste.jokenpo.model.JogadorRequest;
import com.teste.jokenpo.service.*;
import com.teste.jokenpo.service.impl.JogoServiceImpl;
import com.teste.jokenpo.util.GerenciadorArquivos;

import static org.junit.Assert.*;

import java.util.List;

public class JogoServiceImplTest {
  
  JogoService jogoService;
  
  public JogoServiceImplTest() {	
	this.jogoService = new JogoServiceImpl();
  }

  @Test 
  public void testCadastrarJogador() { 
	  
	JogadorRequest jogador = new JogadorRequest();
	jogador.setNumero("1234");
	jogador.setNome("Jo�o");
	Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
	apagaArquivos();
	assertEquals(jogador.getNumero(), jogadorCadastro.getNumero()); 
	assertEquals(jogador.getNome(), jogadorCadastro.getNome()); 
	
  } 
  
  @Test 
  public void testRecuperarJogadores() { 
	
	JogadorRequest jogador = new JogadorRequest();
	jogador.setNumero("12346");
	jogador.setNome("Jo�o");
	jogoService.cadastrarJogador(jogador);

	List<Jogador> jogadores = jogoService.recuperarJogadores();
	apagaArquivos();
	assertTrue(!jogadores.isEmpty());
	
  } 
  
  @Test
  public void testAlterarJogador() {
	 
	JogadorRequest jogador = new JogadorRequest();
	jogador.setNumero("12347");
	jogador.setNome("Jo�o");
	jogoService.cadastrarJogador(jogador);
		
	jogador = new JogadorRequest();
	jogador.setNumero("12347");
	jogador.setNome("Maria");
	Jogador jogadorCadastro = jogoService.alterarJogador(jogador);
	apagaArquivos();
	assertEquals(jogador.getNumero(), jogadorCadastro.getNumero()); 
	assertEquals(jogador.getNome(), jogadorCadastro.getNome()); 
		  
  }
  
  @Test
  public void testConsultaJogador() {
	  
	JogadorRequest jogador = new JogadorRequest();
	jogador.setNumero("12348");
	jogador.setNome("Maria");
	Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
	  
	Jogador consultarJogador = jogoService.consultarJogador(jogadorCadastro.getNumero());
	assertEquals(jogadorCadastro.getNumero(), consultarJogador.getNumero());
	apagaArquivos();
	assertEquals("Maria", consultarJogador.getNome());
	  
  }  
    
  @Test
  public void testJogadaPedraPrimeiroJogador() {
	  
	  JogadorRequest jogador = new JogadorRequest();
	  jogador.setNumero("1239");
	  jogador.setNome("Maria");
	  Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
		
	  JogadaRequest jogadaRequest = new JogadaRequest();
	  jogadaRequest.setCodigoJogador(jogadorCadastro.getNumero());
	  jogadaRequest.setJogada("pedra");
	  
	  String resultadoJogada = jogoService.jokenpo(jogadaRequest);
	  apagaArquivos();
	  assertEquals("pr�ximo jogador", resultadoJogada);
	  
  }
  
  @Test
  public void testJogadaPapelSegundoJogador() {
	  
	  JogadorRequest jogador = new JogadorRequest();
	  jogador.setNumero("2239");
	  jogador.setNome("Jorge");
	  Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
		
	  JogadaRequest jogadaRequest = new JogadaRequest();
	  jogadaRequest.setCodigoJogador(jogadorCadastro.getNumero());
	  jogadaRequest.setJogada("papel");
	  
	  String resultadoJogada = jogoService.jokenpo(jogadaRequest);
	  apagaArquivos();
	  assertEquals("pr�ximo jogador", resultadoJogada);
	  
  }
  
  @Test
  public void testJogadaPapelTerceiroJogador() {
	  
	  JogadorRequest jogador = new JogadorRequest();
	  jogador.setNumero("3239");
	  jogador.setNome("Marcos");
	  Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
	  
	  JogadaRequest jogadaRequest = new JogadaRequest();
	  jogadaRequest.setCodigoJogador(jogadorCadastro.getNumero());
	  jogadaRequest.setJogada("papel");
	  
	  String resultadoJogada = jogoService.jokenpo(jogadaRequest);
	  apagaArquivos();
	  assertEquals("1234 - Maria vencedor!", resultadoJogada);
	  
  }
  
  @Test
  public void testExcluirJogador() {
	
	JogadorRequest jogador = new JogadorRequest();
	jogador.setNumero("5678");
	jogador.setNome("Jorge");
	Jogador jogadorCadastro = jogoService.cadastrarJogador(jogador);
	
	boolean excluirJogador = jogoService.excluirJogador(jogadorCadastro.getNumero());
	apagaArquivos();
	assertTrue(excluirJogador);
	  
  }
  
  private void apagaArquivos() {
	  GerenciadorArquivos.apagarArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES);
	  GerenciadorArquivos.apagarArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGO);
  }
  
}
 