package jokenpo.util.test;

import static org.junit.Assert.assertTrue;

import java.util.ArrayList;

import org.junit.Test;

import com.teste.jokenpo.util.GerenciadorArquivos;

public class GerenciadorArquivosTest {

	public static String GERENCIADOR_ARQUIVOS_TEST_TXT = "gerenciador_arquivos_test.txt";
	
	@Test 
	public void testGravarArquivoBinario() {
		
		boolean gravarArquivoBinario = GerenciadorArquivos.gravarArquivoBinario(new ArrayList<Object>(), GERENCIADOR_ARQUIVOS_TEST_TXT);
		assertTrue(gravarArquivoBinario);
		
	}
	
	@Test 
	public void testLerArquivoBinario() {
		ArrayList<Object> lerArquivoBinario = null;
		if(GerenciadorArquivos.gravarArquivoBinario(new ArrayList<Object>(), GERENCIADOR_ARQUIVOS_TEST_TXT))
			lerArquivoBinario = GerenciadorArquivos.lerArquivoBinario(GERENCIADOR_ARQUIVOS_TEST_TXT);
		apagarArquivoBinario();
		assertTrue(!lerArquivoBinario.isEmpty());		
		
	}
		
	private void apagarArquivoBinario() {		
		GerenciadorArquivos.apagarArquivoBinario(GERENCIADOR_ARQUIVOS_TEST_TXT);
	}
}
