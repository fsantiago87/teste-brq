package com.teste.jokenpo.service;

import java.util.List;

import com.teste.jokenpo.model.JogadaRequest;
import com.teste.jokenpo.model.Jogador;
import com.teste.jokenpo.model.JogadorRequest;

public interface JogoService {
	
	public Jogador cadastrarJogador(JogadorRequest jogador);
	
	public List<Jogador> recuperarJogadores();

	public boolean excluirJogador(String codigo);
	
	public String jokenpo(JogadaRequest jogadaRequest);
	
	public Jogador consultarJogador(String codigo);
	
	public Jogador alterarJogador(JogadorRequest jogadorRequest);

}
