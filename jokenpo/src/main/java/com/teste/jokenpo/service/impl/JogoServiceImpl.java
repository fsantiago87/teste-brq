package com.teste.jokenpo.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.stereotype.Service;

import com.teste.jokenpo.model.Jogada;
import com.teste.jokenpo.model.JogadaRequest;
import com.teste.jokenpo.model.Jogador;
import com.teste.jokenpo.model.JogadorRequest;
import com.teste.jokenpo.service.JogoService;
import com.teste.jokenpo.util.GerenciadorArquivos;

@Service
public class JogoServiceImpl implements JogoService {
	
	private HashMap<String, Map<String, Integer>> inteligenciaJogo;
		
	private int QUANTIDADE_JOGADORES_POR_PARTIDA = 3;
	
	public JogoServiceImpl() {
	/*
	 * Mapa do jogo
	 *			X       spock  lagarto   papel   tesoura  pedra
	 *		
	 *		spock       3        0         0        1        1
	 *		lagarto     1        3         1        0        0
	 *		papel       1        0         3        0        1
	 *		tesoura     0        1         1        3        0 
	 *		pedra       0        1         0        1        3      

	 */
		inteligenciaJogo = new HashMap<String, Map<String, Integer>>();
		
		Map<String, Integer> comparativoMap = new HashMap<String, Integer>();
		comparativoMap.put("spock", 3);
		comparativoMap.put("lagarto", 0);
		comparativoMap.put("papel", 0);		
		comparativoMap.put("tesoura", 1);
		comparativoMap.put("pedra", 1);	
		
		inteligenciaJogo.put("spock", comparativoMap);
		
		
		comparativoMap = new HashMap<String, Integer>();
		comparativoMap.put("spock", 1);
		comparativoMap.put("lagarto", 3);
		comparativoMap.put("papel", 1);
		comparativoMap.put("tesoura", 0);
		comparativoMap.put("pedra", 0);	
		
		inteligenciaJogo.put("lagarto", comparativoMap);
		

		comparativoMap = new HashMap<String, Integer>();
		comparativoMap.put("spock", 1);
		comparativoMap.put("lagarto", 0);
		comparativoMap.put("papel", 3);
		comparativoMap.put("tesoura", 0);
		comparativoMap.put("pedra", 1);	
		
		inteligenciaJogo.put("papel", comparativoMap);
		

		comparativoMap = new HashMap<String, Integer>();
		comparativoMap.put("spock", 0);
		comparativoMap.put("lagarto", 1);
		comparativoMap.put("papel", 1);
		comparativoMap.put("tesoura", 3);
		comparativoMap.put("pedra", 0);	
		
		inteligenciaJogo.put("tesoura", comparativoMap);
		
		
		comparativoMap = new HashMap<String, Integer>();
		comparativoMap.put("papel", 0);
		comparativoMap.put("tesoura", 1);
		comparativoMap.put("pedra", 0);		
		comparativoMap.put("tesoura", 1);
		comparativoMap.put("pedra", 3);	
		
		inteligenciaJogo.put("pedra", comparativoMap);
		
	}
	

	public Jogador cadastrarJogador(JogadorRequest jogadorRequest) {
		Jogador Jogador = new Jogador(jogadorRequest.getNumero(), jogadorRequest.getNome());
		ArrayList<Object> jogadores = GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES);
		jogadores.add(Jogador);
		GerenciadorArquivos.gravarArquivoBinario(jogadores, GerenciadorArquivos.ARQUIVO_JOGADORES);
		return Jogador;
	}
	
	public List<Jogador> recuperarJogadores() {		
		List<Jogador> jogadores = new ArrayList<Jogador>(); 		
		for(Object object: GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES)) {
			Jogador jogador =(Jogador) object;
			jogadores.add(jogador);
		}
		return jogadores;
	}
	
	public Jogador alterarJogador(JogadorRequest jogadorRequest) {
		
		ArrayList<Object> jogadoresAtualizacao = new ArrayList<Object>();
		for(Object object: GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES)) {
			Jogador jogador =(Jogador) object;			
			if(jogador.getNumero().equals(jogadorRequest.getNumero())) {				
				jogadoresAtualizacao.add(new Jogador(jogadorRequest.getNumero(), jogadorRequest.getNome()));
			} else {
				jogadoresAtualizacao.add(jogador);
			}	
		}
		GerenciadorArquivos.gravarArquivoBinario(jogadoresAtualizacao, GerenciadorArquivos.ARQUIVO_JOGADORES);		
		return new Jogador(jogadorRequest.getNumero(), jogadorRequest.getNome());		
	}

	public boolean excluirJogador(String codigo) {
		boolean retorno =false;
		ArrayList<Object> jogadoresAtualizacao = new ArrayList<Object>();
		for(Object object: GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES)) {
			Jogador jogador =(Jogador) object;			
			if(!jogador.getNumero().equals(codigo)) {				
				jogadoresAtualizacao.add(jogador);
				retorno = true;
			}	
		}
		GerenciadorArquivos.gravarArquivoBinario(jogadoresAtualizacao, GerenciadorArquivos.ARQUIVO_JOGADORES);
		
		return retorno;
			
	}

	public Jogador consultarJogador(String codigo) {
		ArrayList<Object> jogadoresRepo = GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGADORES);
		for(Object object: jogadoresRepo) {
			Jogador jogador =(Jogador) object;
			if(jogador.getNumero().equals(codigo)) {				
				return jogador;
			}	
		}
		return null;
			
	}
	
	public String jokenpo(JogadaRequest jogadaRequest) {
		
		String resultado = "Sem resulado";
		int contadorEmpate = 0;
		if(!inteligenciaJogo.containsKey(jogadaRequest.getJogada())) {
			return "jogada n�o cadastrada";
		}
		ArrayList<Object> jogadas = GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGO);
		if(jogadas.isEmpty() || jogadas.size() <QUANTIDADE_JOGADORES_POR_PARTIDA) {
			Jogada jogada = new Jogada();
			jogada.setJogador(new Jogador(jogadaRequest.getCodigoJogador(), this.consultarJogador(jogadaRequest.getCodigoJogador()).getNome()));
			jogada.setJogada(jogadaRequest.getJogada());
			jogadas.add(jogada);
			GerenciadorArquivos.gravarArquivoBinario(jogadas, GerenciadorArquivos.ARQUIVO_JOGO);	
			resultado = "pr�ximo jogador";
		}
		
		if(jogadas.size() == QUANTIDADE_JOGADORES_POR_PARTIDA ) {
			resultado = "sem ganhador";
			jogadas = GerenciadorArquivos.lerArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGO);
			
			Jogada jogada = (Jogada) jogadas.get(0);
			
			int cont = 0;
			for(Object object: jogadas) {
				if(cont == 0) {
					cont++;
					continue;
				}
				Jogada jogadaAux = (Jogada) object;
				int resultadoComp = inteligenciaJogo.get(jogada.getJogada()).get(jogadaAux.getJogada());
				jogada = jogadaAux;
				if(resultadoComp == 1) {
					resultado = jogada.getJogador().getNumero() + " - " + jogada.getJogador().getNome()+ " vencedor!";
					contadorEmpate++;
				}
				cont++;
			}
			
			GerenciadorArquivos.apagarArquivoBinario(GerenciadorArquivos.ARQUIVO_JOGO);
		}
		if(contadorEmpate > 1)
			return "Houve empate na jogada";
		
		return resultado;
	}
}
