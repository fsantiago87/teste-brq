package com.teste.jokenpo.model;

import java.io.Serializable;

public class Jogada implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	
	private Jogador jogador;
	private String jogada;
	
	public Jogador getJogador() {
		return jogador;
	}
	public void setJogador(Jogador jogador) {
		this.jogador = jogador;
	}
	public String getJogada() {
		return jogada;
	}
	public void setJogada(String jogada) {
		this.jogada = jogada;
	}
	
}
