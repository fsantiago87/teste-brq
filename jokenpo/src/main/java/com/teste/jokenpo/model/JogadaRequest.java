package com.teste.jokenpo.model;

public class JogadaRequest {

	public String codigoJogador;
	public String jogada;
	public String getCodigoJogador() {
		return codigoJogador;
	}
	public void setCodigoJogador(String codigoJogador) {
		this.codigoJogador = codigoJogador;
	}
	public String getJogada() {
		return jogada;
	}
	public void setJogada(String jogada) {
		this.jogada = jogada;
	}
	
}
