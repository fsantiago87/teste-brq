package com.teste.jokenpo.model;

import java.io.Serializable;

public class Jogador implements Serializable{
	
	private static final long serialVersionUID = 1L;
	
	private String numero;
	private String nome;
	
	public Jogador(String numero, String nome) {
		super();
		this.numero = numero;
		this.nome = nome;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

}
