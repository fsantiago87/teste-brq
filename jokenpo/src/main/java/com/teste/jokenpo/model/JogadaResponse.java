package com.teste.jokenpo.model;

public class JogadaResponse {

	public String codigoJogador;
	public String jogada;
	public String resultado;
	public String getCodigoJogador() {
		return codigoJogador;
	}
	public void setCodigoJogador(String codigoJogador) {
		this.codigoJogador = codigoJogador;
	}
	public String getJogada() {
		return jogada;
	}
	public void setJogada(String jogada) {
		this.jogada = jogada;
	}
	public String getResultado() {
		return resultado;
	}
	public void setResultado(String resultado) {
		this.resultado = resultado;
	}
	
}
