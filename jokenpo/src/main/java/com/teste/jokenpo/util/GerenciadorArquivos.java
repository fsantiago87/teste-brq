package com.teste.jokenpo.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;


public class GerenciadorArquivos {
	
	public static String ARQUIVO_JOGADORES = "jogadores.txt";
	public static String ARQUIVO_JOGO = "jogo.txt";
	
    public static boolean gravarArquivoBinario(ArrayList<Object> lista, String nomeArq) {
    	File arq = new File(nomeArq);
    	try {
    		arq.delete();
    		arq.createNewFile();
    
    		ObjectOutputStream objOutput = new ObjectOutputStream(new FileOutputStream(arq));
    		objOutput.writeObject(lista);
    		objOutput.close();    
    	} catch(IOException erro) {
    		System.out.printf("Erro: %s", erro.getMessage());
    		return false;
    	}
    	return true;
    }
  
    public static ArrayList<Object> lerArquivoBinario(String nomeArq) {
    	ArrayList<Object> lista = new ArrayList<Object>();
    	try {
    		File arq = new File(nomeArq);
    		if (arq.exists()) {
    			ObjectInputStream objInput = new ObjectInputStream(new FileInputStream(arq));
    			lista = (ArrayList<Object>)objInput.readObject();
    			objInput.close();
    		}
    	} catch(IOException erro1) {
    		System.out.printf("Erro: %s", erro1.getMessage());
    	} catch(ClassNotFoundException erro2) {
    		System.out.printf("Erro: %s", erro2.getMessage());
    	}
    	return(lista);
    }
    
    public static boolean apagarArquivoBinario(String nomeArq) {
    	try {
	    	File arq = new File(nomeArq);
	    	arq.delete();
    	} catch(Exception erro) {
    		System.out.printf("Erro: %s", erro.getMessage());
    		return false;
    	}
    	return true;
    }
	
}
